# -*- coding: utf-8 -*-
# Generated by alex on 2021-02-17
from __future__ import unicode_literals

from django.db import migrations

# detchar is on a tear getting new certs, so I'm doing three
# at once.

gracedb_account = 'detchar'
alex_urban = 'alexander.urban.robot'

new_certs = ['/DC=org/DC=cilogon/C=US/O=LIGO/OU=Robots/CN=detchar.ligo.caltech.edu/CN=detchar/CN=Robert Bruntz/CN=UID:robert.bruntz.robot',
'/DC=org/DC=cilogon/C=US/O=LIGO/OU=Robots/CN=detchar.ligo-la.caltech.edu/CN=detchar-la/CN=Robert Bruntz/CN=UID:robert.bruntz.robot',
'/DC=org/DC=cilogon/C=US/O=LIGO/OU=Robots/CN=detchar.ligo.caltech.edu/CN=detchar_cit/CN=Robert Bruntz/CN=UID:robert.bruntz.robot',
'/DC=org/DC=cilogon/C=US/O=LIGO/OU=Robots/CN=detchar.ligo-wa.caltech.edu/CN=detchar_ligo-wa/CN=Robert Bruntz/CN=UID:robert.bruntz.robot',
]


def add_cert(apps, schema_editor):
    RobotUser = apps.get_model('auth', 'User')

    # Get user
    user = RobotUser.objects.get(username=gracedb_account)

    # delete old certificates:
    alex_certs= user.x509cert_set.filter(subject__contains=alex_urban)
    alex_certs.delete()

    # Create new certificates
    for cert in new_certs:
        user.x509cert_set.create(subject=cert)



def delete_cert(apps, schema_editor):
    RobotUser = apps.get_model('auth', 'User')

    # Get user
    user = RobotUser.objects.get(username=gracedb_account)

    # Delete new certificates
    for cert in new_certs:
      cert = user.x509cert_set.get(subject=cert)
      cert.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0072_catalogdevcron_robot'),
    ]

    operations = [
        migrations.RunPython(add_cert, delete_cert),
    ]
