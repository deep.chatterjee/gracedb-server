# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-08-08 18:27
from __future__ import unicode_literals

from django.db import migrations

GROUP_NAME = 'grb_managers'
USERS = [
    'robert.coyne@LIGO.ORG',
    'ryan.fisher@LIGO.ORG',
    'francesco.pannarale@LIGO.ORG',
    'jordan.palamos@LIGO.ORG',
    'ronaldas.macas@LIGO.ORG',
    'andrew.williamson@LIGO.ORG',
    'iain.dorrington@LIGO.ORG',
]


def add_users(apps, schema_editor):
    AuthGroup = apps.get_model('ligoauth', 'AuthGroup')
    User = apps.get_model('auth', 'User')

    group = AuthGroup.objects.get(name=GROUP_NAME)
    for username in USERS:
        user, _ = User.objects.get_or_create(username=username)
        group.user_set.add(user)


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0050_create_grb_managers_authgroup'),
    ]

    operations = [
        migrations.RunPython(add_users, migrations.RunPython.noop),
    ]
