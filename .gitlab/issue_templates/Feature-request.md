## Description of feature request
<!--
Describe your feature request!
Is it a web interface change? Some underlying feature? An API resource?
The more detail you can provide, the better.
-->


## Use cases
<!-- List some specific cases where this feature will be useful -->


## Benefits
<!-- Describe the benefits of adding this feature -->


## Drawbacks
<!--
Are there any drawbacks to adding this feature?
Can you think of any ways in which this will negatively affect the service for any set of users?
-->


## Suggested solutions
<!-- Do you have any ideas for how to implement this feature? -->
